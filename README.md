# gtkmm-test

Test project of gtkmm on Windows.

## Build

### Install msys2

Download the installer from https://www.msys2.org.

### Install packages

Run MSYS2 MINGW64 shell.

```
pacman -S mingw-w64-x86_64-gtkmm4
pacman -S pkg-config
pacman -S make
pacman -S gcc
pacman -S mingw-w64-x86_64-dbus
```

### Download source code

```
pacman -S git
mkdir git
cd git
git clone https://gitlab.com/hakabahitoyo/gtkmm-test.git
cd gtkmm-test
```